const mongoose = require('mongoose')
const db = 'mongodb://localhost:27017/smile-vue'

// node的glob模块允许使用*等符号，来写一个glob规则，像在shell里一样获取匹配对应规则文件
const glob = require('glob')
// 将一系列路径或路径段解析为绝对路径
const {resolve} = require('path')

exports.initSchema = () => {
  // 引入所有的Schema
  glob.sync(resolve(__dirname, './schema', '**/*.js')).forEach(require)
}

exports.connect = () => {
  // 链接数据库
  mongoose.connect(db,{useNewUrlParser: true})
  let maxConnectTimes = 0
  return new Promise((resolve, reject) =>{
    // 增加数据库的监听事件
    mongoose.connection.on('disconnected', (err) => {
      console.log('数据库断开连接，正在重新连接...')
      if (maxConnectTimes <= 3) { // 如果重连不大于3次就重连
        maxConnectTimes ++
        mongoose.connect(db,{useNewUrlParser: true})
      } else {
        reject(err)
        throw new Error('重连数据库次数超过3次,请检查数据库服务是否正确打开!')
      }
    })
    mongoose.connection.on('error', () => {
      console.log('数据库发生错误，正在重新连接...')
      if (maxConnectTimes <= 3) { // 如果重连不大于3次就重连
        maxConnectTimes ++
        mongoose.connect(db,{useNewUrlParser: true})
      } else {
        reject(err)
        throw new Error('数据库发生未知错误，请联系人工协助!')
      }
    })
    mongoose.connection.once('open', () => {
      console.log('数据库已连接')
      resolve()
    })
  })

}