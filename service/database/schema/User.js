const mongoose = require('mongoose')
const Schema =mongoose.Schema
let ObjectId = Schema.Types.ObjectId
// 加盐加密
const bcrypt = require('bcrypt')
// 加盐长度
const SALT_WORK_FACTOR = 10

// 创建UserSchema
const userSchema = new Schema({
  UserId: ObjectId,
  userName: {
    unique: true,
    type: String
  },
  password : String,
  createAt: {
    type:Date,
    default:Date.now()
  },
  lastLoginAt: {
    type:Date,
    default:Date.now()
  }
}, {
  collection: 'user'
})
// 每次保存之前
userSchema.pre('save', function (next) {
  // 加盐
  bcrypt.genSalt(SALT_WORK_FACTOR, (err, salt)=>{
    if (err) { return next(err) }
    // 加密
    bcrypt.hash(this.password, salt, (err, hash) =>{
      if (err) { return next(err) }
      this.password = hash
      next()
    })
  })
})

// 发布模型
mongoose.model('User',userSchema)