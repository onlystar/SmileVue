const Koa = require('koa')
const app = new Koa()
const {connect, initSchema} = require('./database/init.js')

const bodyParser = require('koa-bodyparser')
const Router = require('koa-router')
const cors = require('koa2-cors')

let user = require('./appApi/user.js')
let home = require('./appApi/home.js')

/**
 * 用来解析body的中间件，通过post来传递表单，json数据，或者上传文件，在koa中是不容易获取的，
 * 通过koa-bodyparser解析之后，在koa中this.body就能直接获取到数据
 */
app.use(bodyParser())
// 解决跨域
app.use(cors())

// 装载所有子路由
let router = new Router()
router.use('/user',user.routes())
router.use('/home',home.routes())

// 加载路由中间件
app.use(router.routes())
app.use(router.allowedMethods())


/**
 * 匿名函数自调方法一
 * 在前加上; 需要()包裹
 */
;(async () => {
  await connect()
  initSchema()
  // const User = mongoose.model('User')
  // let oneUser = new User ({
  //   userName: 'rain',
  //   password: '123456',
  // })
  // oneUser.save().then(()=>{
  //   console.log('数据插入成功')
  // })
  // let user = await User.findOne({}).exec()
  // console.log(user)
})()

// ;(async function abc() {
//   await connect()
// })()
/**
 * 匿名函数自调方法二
 * 在前加上！ + - || 等一元操作符
 * ES6写法需要()包裹
 */
// !(async () => {
//   await connect()
// })()

// !async function abc() {
//   await connect()
// }()

app.use(async (ctx) => {
  ctx.body = '<h1>Hello Rain</h1>'
})

app.listen(3000, () => {
  console.log('server listening at 3000')
})