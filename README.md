# SmileVue

> 这是一个课程项目，使用了Vue的技术，完成移动端商城的基本功能。课程超过20小时的内容，

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```